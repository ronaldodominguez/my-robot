# FonBot 1.0

## Descrição

Robô desenvolvido em **Java** com o foco principal de atacar e "rolar"!
Com sua movimentação para os lados enquanto atira rapidamente, acaba sendo uma ótima arma mortífera!

**Pontos Fortes:** Mais facilidade em desviar das balas dos inimigos

**Pontos Fracos:** Dificuldade em localizar em alguns momentos os inimigos

## O que eu mais gostei de fazer:

Todo o projeto em si foi incrivel, nunca tinha usado essa ferramenta e foram horas quebrando a cabeça para criar um robo que fosse a minha cara: "Louco"

Além de me divertir e torcer para que meu robo não travasse num canto e não saisse (Coisa que aconteceu muitas vezes rsrs)
