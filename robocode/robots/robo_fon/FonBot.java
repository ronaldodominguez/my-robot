package robo_fon;

import java.awt.*;

import robocode.HitByBulletEvent;
import robocode.HitWallEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

public class FonBot extends Robot {

	/**
	 * run: MyFirstRobot's default behavior
	 */
	public void run() {
		boolean angulo = true;
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		setBodyColor(Color.red); // Body => Corpo
		setGunColor(Color.black); // Gun => Arma
		setRadarColor(Color.red); // Radar => Radar

		setScanColor(Color.blue); // Scan => Varredura
		setBulletColor(Color.red); // Bullet => Bala

		// Robot main loop
		while (true) {
			if (angulo) {
				for (int voltasDir = 0; voltasDir < 5; voltasDir++) {

					ahead(150);
					turnRight(90);
					turnGunRight(360);

				}

				for (int voltasEsq = 0; voltasEsq < 4; voltasEsq++) {

					ahead(150);
					turnLeft(90);
					turnGunLeft(360);

				}
				angulo = false;
			} else {
				for (int voltasDir45 = 0; voltasDir45 < 8; voltasDir45++) {

					ahead(50);
					turnRight(45);
					turnGunRight(180);

				}

				for (int voltasEsq45 = 4; voltasEsq45 >= 0; voltasEsq45--) {

					ahead(50);
					turnLeft(45);
					turnGunLeft(180);

				}
				angulo = true;
			}
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		fire(1);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		turnLeft(90 - e.getBearing());

	}

	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(40 - e.getBearing());
	}
}
